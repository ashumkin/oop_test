unit petalclass;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics;

type

  { TPetal }

  TPetal = class
    private
      R, phi: double;
      X, Y, CX, CY: integer;
      Scale, RColor, PetalI: integer;
    public
      constructor Create (Xmax, Ymax: integer);
      procedure Draw (Canvas: TCanvas; Erase: boolean = FALSE);
  end;

implementation

{ TPetal }

constructor TPetal.Create(Xmax, Ymax: integer);
begin
     inherited Create;
     CX:=Random(Xmax);
     CY:=Random(Ymax);
     RColor:=1+Random($FFFFFF);
     Scale:=2+Random(12);
     PetalI:=2+Random(6);
end;

procedure TPetal.Draw(Canvas: TCanvas; Erase: boolean);
begin
     phi:=0;
     if Erase then RColor:=clBlack;
     with Canvas do
       while phi < 2*pi do
         begin
           R:=10*sin(PetalI*phi);
           X:=CX+Trunc(Scale*R*cos(phi));
           Y:=CY-Trunc(Scale*R*sin(phi));
           Pixels[X,Y]:=RColor;
           phi+=pi/1800;
         end;
end;

end.

